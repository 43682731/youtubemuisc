from PyQt5.QtCore import *
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import *
from PyQt5.QtWebEngineWidgets import *
from pynput.keyboard import *
from pywinauto import application
from pywinauto.findwindows import WindowAmbiguousError, WindowNotFoundError

import sys
import threading
'''
=========================================
        Youtube Desktop App
==========================================
'''


# Quick little icon workaround
import ctypes

app_id = 'youtube.music.player'  # arbitrary string
ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(app_id)

# Globals
pywin_app = application.Application()


# Window Setup
class Window(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(Window, self).__init__(*args, **kwargs)
        self.setWindowTitle("Youtube Music")
        self.resize(1000, 700)
        self.setWindowIcon(QIcon("icon.ico"));
        self.browser = QWebEngineView()
        self.browser.setUrl(QUrl("https://music.youtube.com/"))
        self.setCentralWidget(self.browser)

    def closeEvent(self, *args, **kwargs):
        self.browser.close()
        sys.exit()


def on_key_press(key):
    key_str = str(key)
    if key_str == '<179>' or key_str == '<177>' or key_str == '<176>':
        try:
            pywin_app.connect(title_re=".*Youtube Music*", visible_only=True)
            if key_str == '<179>':
                pywin_app.window(title_re=".*Youtube Music*", visible_only=True).send_keystrokes(" ")
            elif key_str == '<177>':
                pywin_app.window(title_re=".*Youtube Music*", visible_only=True).send_keystrokes("k")
            elif key_str == '<176>':
                pywin_app.window(title_re=".*Youtube Music*", visible_only=True).send_keystrokes("j")
        except WindowNotFoundError:
            print("Not Found")
            pass
        except WindowAmbiguousError:
            print("Ambiguous Error")
            pass


def main():
    main_app = QApplication(sys.argv)
    window = Window()
    window.show()
    main_app.exec()


def listen():
    while True:
        with Listener(on_press=on_key_press) as listener:
            listener.join()


if __name__ == '__main__':
    threading.Thread(target=main).start()
    threading.Thread(target=listen).start()
