# README #

Install required libaries and run it.

# Executable Download #

https://www.dropbox.com/sh/ftjxxb6d4ra6kqw/AADwAZe2yBJXTXEqSJvWTDt1a?dl=0

# Creating your own Executable #

If you wish to export to an exe pyintaller is a useful tool.
Once you have installed pyinstaller cd to the src directory and then type: pyintaller -i "C:\Path_to_icon_file.ico" -F -w
This will create a single exe with an icon.
The final step is to ensure that a file called icon.ico is present in the same directory as your YoutubeMusic.exe file
